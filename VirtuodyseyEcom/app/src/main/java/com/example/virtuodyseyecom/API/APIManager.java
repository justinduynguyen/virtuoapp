package com.example.virtuodyseyecom.API;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.virtuodyseyecom.Activities.ProductDetail;
import com.example.virtuodyseyecom.Adapters.ClickListener;
import com.example.virtuodyseyecom.Adapters.RecycleApdater;
import com.example.virtuodyseyecom.Model.Product;
import com.example.virtuodyseyecom.Singleton.CenterRepository;
import com.example.virtuodyseyecom.Uitlity.CenterZoomLayoutManager;
import com.example.virtuodyseyecom.Uitlity.ConnectionString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class APIManager {

    private Context context;
    public List<Product> products = new ArrayList<>();
    private static APIManager instance;

    public APIManager(Context context) {
        this.context = context;
    }


    public static APIManager getInstance(Context context) {
        if (null == instance) {
            instance = new APIManager(context);
        }
        instance.context = context;
        return instance;

    }

    public void GetProduct(final int page, final int number, final RecyclerView recyclerView, final String key) throws JSONException {

        if (CenterRepository.getInstance().getLoadedPage().get(key) != null)
            if (CenterRepository.getInstance().getLoadedPage().get(key).contains(page)) {
                RecycleApdater adapter = new RecycleApdater(context, CenterRepository.getInstance().getMapProductInCategory().get(key), new ClickListener() {
                    @Override
                    public void onPositionClicked(int position) {
                        Intent intent = new Intent(context, ProductDetail.class);
                        intent.putExtra("product_id", position);
                        context.startActivity(intent);
                    }

                    @Override
                    public void onLongClicked(int position) {

                    }
                });
                recyclerView.setAdapter(adapter);
                return;
            }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConnectionString.Connection +
                "list_product/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray json = jsonObject.getJSONArray("data");

                    for (int i = 0; i < json.length(); i++) {
                        String id = json.getJSONObject(i).has("ID") ? json.getJSONObject(i).getString("ID") : "null";
                        String name = json.getJSONObject(i).has("product_name") ? json.getJSONObject(i).getString("product_name") : "null";
                        String url = json.getJSONObject(i).has("url_img") ? json.getJSONObject(i).getString("url_img") : "null";
                        String des = json.getJSONObject(i).has("description") ? json.getJSONObject(i).getString("description") : "null";
                        float price = json.getJSONObject(i).has("price") ? Float.parseFloat(json.getJSONObject(i).getString("price")) : 0.0f;
                        boolean limit = json.getJSONObject(i).has("limit") ? (boolean) json.getJSONObject(i).getBoolean("limit") : false;
                        Product product = new Product(id, name, url, des, price, limit);
                        products.add(product);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                CenterRepository.getInstance().AssignProductInCatgory(key, page, products);
                if (CenterRepository.getInstance().getMapProductInCategory().get(key).size() > 0) {
                    RecycleApdater adapter = new RecycleApdater(context, CenterRepository.getInstance().getMapProductInCategory().get(key), new ClickListener() {
                        @Override
                        public void onPositionClicked(int position) {
                            if (CenterRepository.getInstance().getMapProductInCategory().get(key) != null) {
                                Intent intent = new Intent(context, ProductDetail.class);
                                intent.putExtra("product_id", position);

                                context.startActivity(intent);
                            }
                        }

                        @Override
                        public void onLongClicked(int position) {

                        }
                    });
                    recyclerView.setAdapter(adapter);

                    final CenterZoomLayoutManager layoutManager = new CenterZoomLayoutManager(context, 0.5f, .9f);

                    recyclerView.setLayoutManager(layoutManager);

                    recyclerView.setHasFixedSize(true);
                    final SnapHelper snapHelper = new LinearSnapHelper();

                    snapHelper.attachToRecyclerView(recyclerView);
                    layoutManager.scrollToPosition(1);

                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            View view = layoutManager.findViewByPosition(1);
                            if (view == null) {
                                // Log.e(WingPickerView.class.getSimpleName(), "Cant find target View for initial Snap");
                                return;
                            }

                            int[] snapDistance = snapHelper.calculateDistanceToFinalSnap(layoutManager, view);
                            if (snapDistance[0] != 0 || snapDistance[1] != 0) {
                                recyclerView.smoothScrollBy(snapDistance[0], snapDistance[1]);
                            }
                        }
                    });

                    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(@NonNull final RecyclerView recyclerView, int newState) {
                            super.onScrollStateChanged(recyclerView, newState);
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                View centerView = snapHelper.findSnapView(layoutManager);
                                int pos = layoutManager.getPosition(centerView);
                                Log.d("pos: ", String.valueOf(pos));
                               if(pos==0)
                               {
                                   recyclerView.post(new Runnable() {
                                       @Override
                                       public void run() {
                                           View view = layoutManager.findViewByPosition(1);
                                           if (view == null) {
                                               // Log.e(WingPickerView.class.getSimpleName(), "Cant find target View for initial Snap");
                                               return;
                                           }

                                           int[] snapDistance = snapHelper.calculateDistanceToFinalSnap(layoutManager, view);
                                           if (snapDistance[0] != 0 || snapDistance[1] != 0) {
                                               recyclerView.smoothScrollBy(snapDistance[0], snapDistance[1]);
                                           }
                                       }
                                   });
                               }
                               if(pos==products.size()-1)
                               {
                                   recyclerView.post(new Runnable() {
                                       @Override
                                       public void run() {
                                           View view = layoutManager.findViewByPosition(products.size()-2);
                                           if (view == null) {
                                               // Log.e(WingPickerView.class.getSimpleName(), "Cant find target View for initial Snap");
                                               return;
                                           }

                                           int[] snapDistance = snapHelper.calculateDistanceToFinalSnap(layoutManager, view);
                                           if (snapDistance[0] != 0 || snapDistance[1] != 0) {
                                               recyclerView.smoothScrollBy(snapDistance[0], snapDistance[1]);
                                           }
                                       }
                                   });
                               }
                            }
                        }
                    });

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("per_page", String.valueOf(number));
                params.put("paged", String.valueOf(page));
                return params;
            }

        };

        RequestManagerSingleton.getInstance(this.context).addToRequestQueue(stringRequest);


    }


}
