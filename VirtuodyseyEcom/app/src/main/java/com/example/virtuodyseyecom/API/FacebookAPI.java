package com.example.virtuodyseyecom.API;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;

public class FacebookAPI {
    public static boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
public  static void LogoutFaceBook()
{
    if(isLoggedIn())
    LoginManager.getInstance().logOut();
}
}
