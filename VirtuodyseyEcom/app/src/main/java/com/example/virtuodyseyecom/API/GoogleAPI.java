package com.example.virtuodyseyecom.API;

import android.content.Context;

import com.example.virtuodyseyecom.Activities.SignUp_Login;
import com.example.virtuodyseyecom.R;
import com.example.virtuodyseyecom.Uitlity.TextUtilities;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import static com.example.virtuodyseyecom.Activities.SignUp_Login.gso;

public class GoogleAPI {

   public static void googleSignout(Context context)
   {
       gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
               .requestIdToken(String.valueOf(R.string.server_client_id)).requestEmail()
               .build();

// Build a GoogleSignInClient with the options specified by gso.
       SignUp_Login.mGoogleSignInClient = GoogleSignIn.getClient(context, SignUp_Login.gso);
       SignUp_Login.mGoogleSignInClient.signOut();
   }
}
