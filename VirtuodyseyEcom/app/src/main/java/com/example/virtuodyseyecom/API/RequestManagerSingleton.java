package com.example.virtuodyseyecom.API;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RequestManagerSingleton {
    private RequestQueue queue;
    public static RequestManagerSingleton instance;
    private Context context;


    public RequestManagerSingleton(Context context) {
        this.context = context;
        queue = getRequestQueue();
    }

    public static synchronized RequestManagerSingleton getInstance(Context context) {
        if (null == instance) {
            instance = new RequestManagerSingleton(context.getApplicationContext());
        }
        return instance;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(this.context);
        }
        return queue;
    }


}
