package com.example.virtuodyseyecom.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.virtuodyseyecom.API.APIManager;
import com.example.virtuodyseyecom.API.FacebookAPI;
import com.example.virtuodyseyecom.API.GoogleAPI;
import com.example.virtuodyseyecom.Controllers.ViewInflateUtilities;
import com.example.virtuodyseyecom.R;
import com.example.virtuodyseyecom.Uitlity.ConnectionString;
import com.example.virtuodyseyecom.Uitlity.TextUtilities;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.unity.ARProject.UnityPlayerActivity;

import org.json.JSONException;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public HomeActivity() {
    }
    public static void Launch(Activity activity)
    {
        activity.startActivity(new Intent(activity,HomeActivity.class));
    }
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView txtLogin;
    RecyclerView recyclerView;
    ImageView imageAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);
        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // set drawerlayout
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        recyclerView = findViewById(R.id.recycleView);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        txtLogin = navigationView.getHeaderView(0).findViewById(R.id.btnLogin);
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent itent = new Intent(HomeActivity.this, SignUp_Login.class);
                startActivity(itent);
                drawer.closeDrawer(Gravity.START, false);
            }
        });


//        // Load product to recycle view

        addMenuToNavigationHome(navigationView);
//        final CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.VERTICAL, false);
//        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());


        try {
            APIManager.getInstance(this).GetProduct(1, 54, recyclerView, "General");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Login request
        Intent intent = getIntent();
        if (FacebookAPI.isLoggedIn() || isLoggedGoogle() || intent.getBooleanExtra("loginFacebook", false) || intent.getBooleanExtra("loginGoogle", false)) {
            ViewInflateUtilities.getInstance(this).setHeaderView(navigationView, R.layout.nav_header_home);
            imageAvatar = navigationView.getHeaderView(0).findViewById(R.id.imageAvatar);
            imageAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent =new Intent(HomeActivity.this,UserProfile.class);
                    startActivity(intent);
                }
            });
        }


    }

    boolean isLoggedGoogle() {

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account == null) return false;
        return true;

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    void addMenuToNavigationHome(NavigationView view) {
        Menu menu = view.getMenu();

        SubMenu subMenu = menu.addSubMenu(TextUtilities.SetTextStyle(getBaseContext(), "Category", 1.5f));

        subMenu.add(0, Menu.FIRST, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    AR", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 1, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Street Wear", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 2, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Unisex", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 3, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Hot Summner ", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 4, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Free Style", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 5, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    T-Shirt", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 6, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    AR", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 7, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Street Wear", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 8, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Unisex", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 9, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Hot Summner ", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 10, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Free Style", 1.5f))
        ;
        subMenu.add(0, Menu.FIRST + 11, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    T-Shirt", 1.5f))
        ;
        if (FacebookAPI.isLoggedIn() || isLoggedGoogle()) {
            SubMenu subMenu_1 = menu.addSubMenu(TextUtilities.SetTextStyle(getBaseContext(), " ", 1.5f));
            subMenu_1.add(1, Menu.FIRST + 13, Menu.FIRST, TextUtilities.SetTextStyle(getBaseContext(), "    Log out", 1.5f))
                    .setIcon(R.drawable.logout_icon);
        }


    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        TextUtilities.MakeToast(getBaseContext(), String.valueOf(item.getItemId()));
        switch (item.getItemId()) {
            case 14:
                FacebookAPI.LogoutFaceBook();
                GoogleAPI.googleSignout(getBaseContext());
                ConnectionString.loginFacebook = false;
                ConnectionString.loginGoogle = false;
                ViewInflateUtilities.getInstance(getBaseContext()).setHeaderView(navigationView, R.layout.nav_header);
                navigationView.setNavigationItemSelectedListener(this);
                txtLogin = navigationView.getHeaderView(0).findViewById(R.id.btnLogin);
                txtLogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent itent = new Intent(HomeActivity.this, SignUp_Login.class);
                        startActivity(itent);
                        drawer.closeDrawer(Gravity.START, false);

                    }

                });

                item.setVisible(false);
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnCamera:
                Intent intent = new Intent(this, UnityPlayerActivity.class);
                startActivity(intent);
                break;
            case R.id.btnCShopCard:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
