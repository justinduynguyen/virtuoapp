package com.example.virtuodyseyecom.Activities;

import android.content.Intent;
import android.support.v4.text.HtmlCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.virtuodyseyecom.Model.Product;
import com.example.virtuodyseyecom.R;
import com.example.virtuodyseyecom.Singleton.CenterRepository;

public class ProductDetail extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtNameProduct;
    TextView txtPriceProduct;
    TextView txtDesProduct;
    ImageView imageProduct;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(ProductDetail.this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //Set reference for view
        toolbar = findViewById(R.id.toolbar);
        txtNameProduct = findViewById(R.id.txtNameProduct);
        txtPriceProduct = findViewById(R.id.txtPriceProduct);
        txtDesProduct = findViewById(R.id.txtDes);
        imageProduct = findViewById(R.id.imageProduct);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        int position = intent.getIntExtra("product_id", 0);
        SetContentToProduct(position);

    }
    void SetContentToProduct(int position)
    {
        Glide.with(this).load(CenterRepository.getInstance().
                getMapProductInCategory().get("General").get(position).getUrl_image()).into(imageProduct);
        txtNameProduct.setText(CenterRepository.getInstance().
                getMapProductInCategory().get("General").get(position).getName());
        txtPriceProduct.setText(String.valueOf(CenterRepository.getInstance().
                getMapProductInCategory().get("General").get(position).getPride()));
        txtDesProduct.setText(HtmlCompat.fromHtml( CenterRepository.getInstance().
                getMapProductInCategory().get("General").get(position).getDes(),0));
    }
}
