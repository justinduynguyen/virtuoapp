package com.example.virtuodyseyecom.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.virtuodyseyecom.API.RequestManagerSingleton;
import com.example.virtuodyseyecom.R;
import com.example.virtuodyseyecom.Uitlity.ConnectionString;
import com.example.virtuodyseyecom.Uitlity.TextUtilities;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SignUp_Login extends AppCompatActivity implements View.OnClickListener {
    private static final int RC_SIGN_IN = 687;
    private static final String EMAIL = "email";
    private static final String USER_POSTS = "user_posts";
    private static final String AUTH_TYPE = "rerequest";
    private Toolbar toolbar;
    private boolean lockback=false;
    private CallbackManager mCallbackManager;
    public static SignInButton btnGoogle;
    public static GoogleSignInOptions gso;
    public static GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        lockback=false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up__login);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mCallbackManager = CallbackManager.Factory.create();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //google config
        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
         gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id)).requestEmail()
                .build();

// Build a GoogleSignInClient with the options specified by gso.
      mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);


        LoginButton mLoginButton = findViewById(R.id.btnlogin_button);

        // Set the initial permissions to request from the user while logging in
        mLoginButton.setReadPermissions(Arrays.asList(EMAIL, USER_POSTS));
        mLoginButton.setAuthType(AUTH_TYPE);

        // Register a callback to respond to the user
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                setResult(RESULT_OK);
                lockback=true;
               LoginFacebook(loginResult.getAccessToken().getToken());


            }

            @Override
            public void onCancel() {
                setResult(RESULT_CANCELED);

            }

            @Override
            public void onError(FacebookException e) {
                // Handle exception
                TextUtilities.MakeToast(getBaseContext(), e.toString());
            }
        });


    }
    @Override
    protected void onStart()
    {
        super.onStart();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(!lockback) {
            Intent intent = new Intent(SignUp_Login.this, HomeActivity.class);
            startActivity(intent);
            overridePendingTransitionExit();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(View view) {
        lockback = true;
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            // ...
        }

    }
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        overridePendingTransitionExit();
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            LoginGoogle(account.getIdToken());

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            TextUtilities.MakeToast(getBaseContext(), "signInResult:failed code=" + e.getStatusCode());

        }
    }
    public void LoginFacebook(final String idToken) {
        StringRequest stringRequest  =new StringRequest(Request.Method.POST, ConnectionString.Connection + "login_facebook", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if(!TextUtilities.isValidJson(jsonObject))
                    {
                        lockback=false;
                        return;
                    }

                    Log.d("Login",jsonObject.toString() +"Token : " +idToken);
                    TextUtilities.MakeToast(getBaseContext(),jsonObject.toString());
                    ConnectionString.loginFacebook = true;
                    Intent intent = new Intent(SignUp_Login.this, HomeActivity.class);
                    intent.putExtra("loginFacebook", ConnectionString.loginFacebook);
                    startActivityForResult(intent, 1);
                    overridePendingTransitionExit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params =new HashMap<>();
                params.put("access_token",idToken);
                return  params;
            }
        };
        RequestManagerSingleton.getInstance(getBaseContext()).addToRequestQueue(stringRequest);

    }

    public void LoginGoogle(final String idToken) {
//        StringRequest stringRequest  =new StringRequest(Request.Method.POST, ConnectionString.Connection + "login", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                JSONObject jsonObject = null;
//                try {
//                    jsonObject = new JSONObject(response);
//
//                    if(!TextUtilities.isValidJson(jsonObject)) {
//                        lockback=false;
//                        return;
//                    }
//
//
//                    Log.d("Login",jsonObject.toString() +"Google Token : " +idToken);
//                    TextUtilities.MakeToast(getBaseContext(),jsonObject.toString());
//                    ConnectionString.loginGoogle = true;
//                    Intent intent = new Intent(SignUp_Login.this, HomeActivity.class);
//                    intent.putExtra("loginGoogle", ConnectionString.loginGoogle);
//                    startActivityForResult(intent, 1);
//                    overridePendingTransitionExit();
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> params =new HashMap<>();
//                params.put("id_token",idToken);
//                return  params;
//            }
//        };
//        RequestManagerSingleton.getInstance(getBaseContext()).addToRequestQueue(stringRequest);
        TextUtilities.MakeToast(this.getBaseContext(),idToken);
        Intent intent = new Intent(SignUp_Login.this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransitionExit();
    }
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }
}



