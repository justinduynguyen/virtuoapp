package com.example.virtuodyseyecom.Adapters;

public interface ClickListener {

    void onPositionClicked(int position);

    void onLongClicked(int position);
}
