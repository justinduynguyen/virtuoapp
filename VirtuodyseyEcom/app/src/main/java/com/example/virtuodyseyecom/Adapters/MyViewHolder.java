package com.example.virtuodyseyecom.Adapters;

import android.os.Debug;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.virtuodyseyecom.R;
import com.example.virtuodyseyecom.Singleton.CenterRepository;
import com.example.virtuodyseyecom.Uitlity.TextUtilities;

import java.lang.ref.WeakReference;

public  class MyViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener,View.OnLongClickListener
{

    TextView txtTitle;
    ImageView imageView;
    TextView txtPrice;
    private WeakReference<ClickListener> listenerRef;
    public  MyViewHolder(View itemView,ClickListener clickListener)
    {
        super(itemView);
        listenerRef=new WeakReference<>(clickListener);
        txtTitle=itemView.findViewById(R.id.txtTitle);
        txtPrice=itemView.findViewById(R.id.txtPrice);
        imageView =itemView.findViewById(R.id.imageProduct);
        imageView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==imageView.getId())
        {

        }
        listenerRef.get().onPositionClicked(getAdapterPosition());
    }

    @Override
    public boolean onLongClick(View view) {
        listenerRef.get().onLongClicked(getAdapterPosition());
        return true;
    }
}