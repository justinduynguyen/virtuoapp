package com.example.virtuodyseyecom.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.virtuodyseyecom.Model.Product;
import com.example.virtuodyseyecom.R;

import java.util.List;


public class RecycleApdater extends RecyclerView.Adapter<MyViewHolder> {
    private List<Product> productList;
    private Context context;
    private ClickListener clickListener;
    public RecycleApdater(Context context, List<Product> list,ClickListener clickListener) {
        this.context = context;
        this.productList = list;
        this.clickListener=clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view;
        view = inflater.inflate(R.layout.product_item, viewGroup, false);
        return new MyViewHolder(view,clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.txtTitle.setText(productList.get(i).getName());
        myViewHolder.txtPrice.setText(Float.toString(productList.get(i).getPride()) + " $");
        Glide.with(context).load(productList.get(i).getUrl_image()).into(myViewHolder.imageView);

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


}

