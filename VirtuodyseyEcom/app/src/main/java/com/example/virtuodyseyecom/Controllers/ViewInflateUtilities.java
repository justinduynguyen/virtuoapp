package com.example.virtuodyseyecom.Controllers;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.View;

import com.example.virtuodyseyecom.R;

import java.util.zip.Inflater;

public class ViewInflateUtilities {
    private  static  ViewInflateUtilities instance;
    private Context context;
    public  ViewInflateUtilities(Context context)
    {
        this.context= context;
    }
    public static ViewInflateUtilities getInstance(Context context)
    {
        if(instance ==null)
        instance =  new ViewInflateUtilities(context);
      return  instance;
    }

    public  void setHeaderView(View view,int id)
    {
        if(view instanceof NavigationView)
        {
            ((NavigationView)view).removeHeaderView(((NavigationView) view).getHeaderView(0));
            ((NavigationView) view).inflateHeaderView(id);

        }
    }
}
