package com.example.virtuodyseyecom.Model;

public class Product {

    private  String name;
    private  String url_image;
    private  String des;
    private  float pride;
    private  boolean limit;
    private  String productID;
    public  Product()
    {

    }
    public Product(String productID,String name,String url_image,String des,float pride,boolean limit )
    {
        this.des=des;
        this.limit=limit;
        this.productID=productID;
        this.url_image=url_image;
        this.pride=pride;
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl_image() {
        return url_image;
    }

    public void setUrl_image(String url_image) {
        this.url_image = url_image;
    }

    public String getDes() {
        return des;
    }

    public float getPride() {
        return pride;
    }

    public void setPride(float pride) {
        this.pride = pride;
    }

    public boolean isLimit() {
        return limit;
    }

    public void setLimit(boolean limit) {
        this.limit = limit;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }



}
