package com.example.virtuodyseyecom.Singleton;

import com.example.virtuodyseyecom.Model.CategoryProduct;
import com.example.virtuodyseyecom.Model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class CenterRepository {
    public static CenterRepository instance;
    private ArrayList<CategoryProduct> listCategoryProduct = new ArrayList<>();

    public ConcurrentHashMap<String, ArrayList<Integer>> getLoadedPage() {
        return loadedPage;
    }

    public void setLoadedPage(ConcurrentHashMap<String, ArrayList<Integer>> loadedPage) {
        this.loadedPage = loadedPage;
    }

    private ConcurrentHashMap<String,ArrayList<Integer>> loadedPage =new ConcurrentHashMap<>();



    private ConcurrentHashMap<String, ArrayList<Product>> mapProductInCategory = new ConcurrentHashMap<>();
    private List<Product> listOfProductsInShoppingList = Collections.synchronizedList(new ArrayList<Product>());

    public static CenterRepository getInstance() {
        if (null == instance) {
            instance = new CenterRepository();
        }
        return instance;
    }
    public  void AssignProductInCatgory(String key,int page,List<Product> list)
    {
        if (CenterRepository.getInstance().getLoadedPage().get(key)==null ||CenterRepository.getInstance().getMapProductInCategory().get(key)==null)
        {
            CenterRepository.getInstance().getLoadedPage().put(key,new ArrayList<Integer>(Arrays.asList(page)));
            CenterRepository.getInstance().getMapProductInCategory().put(key, (ArrayList<Product>) list);
        }
        else
        {
            if(!CenterRepository.getInstance().getLoadedPage().get(key).contains(page))
            {
                CenterRepository.getInstance().getMapProductInCategory().get(key).addAll(list);
            }
        }


    }
    public void setListCategory(ArrayList<CategoryProduct> listCategory) {
        this.listCategoryProduct = listCategory;
    }

    public void setMapOfProductsInCategory(ConcurrentHashMap<String, ArrayList<Product>> mapOfProductsInCategory) {
        this.mapProductInCategory = mapOfProductsInCategory;

    }

    public void setListOfProductsInShoppingList(ArrayList<Product> listOfProductsInShoppingList) {
        this.listOfProductsInShoppingList = listOfProductsInShoppingList;
    }
    public ArrayList<CategoryProduct> getListCategoryProduct()
    {
        return this.listCategoryProduct;
    }
    public  ConcurrentHashMap<String,ArrayList<Product>> getMapProductInCategory ()
    {
        return  this.mapProductInCategory;
    }
    public  List<Product> getListProductsInShoping()
    {
        return this.listOfProductsInShoppingList;
    }
}
