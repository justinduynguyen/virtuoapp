package com.example.virtuodyseyecom.Uitlity;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class TextUtilities {
    public  static   SpannableString SetTextStyle(Context context,String text,float scale) {
        SpannableString spanString = new SpannableString(text);
        int end = spanString.length();
        spanString.setSpan(
                new RelativeSizeSpan(scale), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return  spanString;
    }
    public static void MakeToast(Context context,String txt)
    {
        Toast.makeText(context.getApplicationContext(),txt,Toast.LENGTH_SHORT).show();
    }
    public  static  boolean isValidJson(JSONObject json)
    {
        if(json.has("status")==false || json.has("check")==false) return  false;
        try {
            return (json.getString("status").equals("ok") && json.getBoolean("check"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  false;
    }

}
